-------------------------------------------------------------------------------
--
-- Title       : RS_FF_BD
-- Design      : RS_FF
-- Author      : Admin
-- Company     : �
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab1\RS_FF\compile\RS_FF_BD.vhd
-- Generated   : Sat Feb  4 12:51:32 2017
-- From        : D:\My_Designs\lab1\RS_FF\src\RS_FF_BD.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_unsigned.all;


entity RS_FF is
  port(
       R : in STD_LOGIC;
       S : in STD_LOGIC;
       NQ : out STD_LOGIC;
       Q : out STD_LOGIC
  );
end RS_FF;

architecture RS_FF_Arch of RS_FF is

---- Component declarations -----

component RS_FF
  port (
       R : in STD_LOGIC;
       S : in STD_LOGIC;
       NQ : out STD_LOGIC;
       Q : out STD_LOGIC
  );
end component;

begin

----  Component instantiations  ----

U1 : RS_FF
  port map(
       NQ => NQ,
       Q => Q,
       R => R,
       S => S
  );


end RS_FF_Arch;
