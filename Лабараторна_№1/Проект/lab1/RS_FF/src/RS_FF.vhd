library IEEE;
use IEEE.std_logic_1164.all;	
use IEEE.STD_LOGIC_UNSIGNED.all;

entity RS_FF is
	port (
		R : in std_logic;
		S : in std_logic;
		Q : out std_logic;
		NQ : out std_logic
		);
end RS_FF;

architecture RS_FF_Arch of RS_FF is
begin						  	
	process (R, S)		
	variable Q0 : STD_LOGIC; 				   
	begin 		       			
		if(R = '0' and S = '0') then
			Q0 := Q0;
		elsif ( R = '1' ) then
			Q0 := '0';
		elsif ( S = '1' ) then
			Q0 := '1';
		end if;
		Q <= Q0;
		NQ <= not Q0;
		
	end process;	
end RS_FF_Arch;