-------------------------------------------------------------------------------
--
-- Title       : RS_FF_Wizard
-- Design      : RS_FF
-- Author      : Admin
-- Company     : �
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab1\RS_FF\src\RS_FF_Wizard.vhd
-- Generated   : Sat Feb  4 13:40:16 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {RS_FF_Wizard} architecture {RS_FF_Wizard}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity RS_FF_Wizard is
	 port(
		 R : in STD_LOGIC;
		 S : in STD_LOGIC;
		 Q : out STD_LOGIC;
		 NQ : out STD_LOGIC
	     );
end RS_FF_Wizard;

--}} End of automatically maintained section

architecture RS_FF_Wizard of RS_FF_Wizard is
begin

	 -- enter your statements here --

end RS_FF_Wizard;
