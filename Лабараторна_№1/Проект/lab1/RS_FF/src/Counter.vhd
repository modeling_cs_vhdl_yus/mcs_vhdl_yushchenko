-------------------------------------------------------------------------------
--
-- Title       : Counter
-- Design      : RS_FF
-- Author      : Admin
-- Company     : �
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab1\RS_FF\src\Counter.vhd
-- Generated   : Sat Feb  4 13:46:11 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Counter} architecture {Counter}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity Counter is
	 port(
		 CLK : in STD_LOGIC;
		 RST : in STD_LOGIC;
		 Q : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end Counter;

--}} End of automatically maintained section

architecture Counter of Counter is
begin
	process(CLK, RST)
	variable Qint : STD_LOGIC_VECTOR (3 downto 0);		 
	begin 		 
		if RST = '1' then
			Qint := "0000";
		 
		 elsif rising_edge(CLK) then		   --�������� �����
			  Qint := Qint+1;
		end if;
		
		Q <= Qint;
	end process;	 				  

end Counter;