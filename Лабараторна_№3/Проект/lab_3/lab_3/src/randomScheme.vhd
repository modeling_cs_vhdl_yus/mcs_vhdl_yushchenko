library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity randomScheme is
	port(
		X : in STD_LOGIC_VECTOR(2 downto 0);
		Y : out STD_LOGIC
		);
end randomScheme;

architecture randomScheme of randomScheme is
	signal A,B,C,D,E,F : STD_LOGIC := '0';
	constant INVERTOR_DELAY : time := 5 ns;
	constant OTHER_DELAY : time := 10 ns;
begin
	
	process(X)	
	begin								 
		A <= transport (not X(0)) after INVERTOR_DELAY;
		B <= transport (A or X(1)) after OTHER_DELAY;
		C <= transport (A nor X(2)) after OTHER_DELAY;
		D <= transport (B and C) after OTHER_DELAY;
		E <= transport (C xnor X(2)) after OTHER_DELAY;
		F <= transport (B xor D) after OTHER_DELAY;
		Y <= transport (F nand E) after OTHER_DELAY;	
	end process;
	
	
end randomScheme;
