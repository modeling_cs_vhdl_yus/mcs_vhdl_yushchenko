-------------------------------------------------------------------------------
--
-- Title       : comp
-- Design      : lab_4
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : d:\My_Designs\lab_4\lab_4\src\comp.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {comp} architecture {comp}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity comp is
	port(
		Bout : out STD_LOGIC;
		Ainout : inout STD_LOGIC
		);
end comp;

--}} End of automatically maintained section

architecture comp of comp is
	signal CLK : std_logic :='0';
begin 
	
	Pr_CLK: process
	begin	
		wait for 10ns;
		CLK <= not CLK;
	end process Pr_CLK;
	
	Pr_A: process
	begin
		wait on CLK;
		if CLK ='1' then Ainout<='1' after 5 ns;
		elsif CLK='0' then Ainout<='0' after 5 ns; 
		end if;
	end process Pr_A;
	
	Pr_B: process 
	begin
		wait until Ainout'event;
		Bout<= not Ainout;
	end process Pr_B; 
	
end comp;
