-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab_7
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab_7\lab_7\compile\ScanerKey.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ScanerKey is 
	port (
		CLK: in STD_LOGIC;
		Ret: in STD_LOGIC_VECTOR (3 downto 0);
		KeyCode: out STD_LOGIC_VECTOR (7 downto 0);
		Strobe: out STD_LOGIC;
		Scan: inout STD_LOGIC_VECTOR (3 downto 0));
end ScanerKey;

architecture ScanerKey_arch of ScanerKey is

-- diagram signals declarations
signal Cound: STD_LOGIC;

-- USER DEFINED ENCODED state machine: Sreg0
attribute ENUM_ENCODING: string;
type Sreg0_type is (
    S2, S4, S1, S5, S0, S3
);
attribute ENUM_ENCODING of Sreg0_type: type is
	"0 " &		-- S2
	"0 " &		-- S4
	"0 " &		-- S1
	"0 " &		-- S5
	"0 " &		-- S0
	"0" ;		-- S3

signal Sreg0: Sreg0_type;

begin


----------------------------------------------------------------------
-- Machine: Sreg0
----------------------------------------------------------------------
Sreg0_machine: process (CLK)
begin
	if rising_edge(CLK) then
		-- Set default values for outputs, signals and variables
		-- ...
		case Sreg0 is
			when S2 =>
				if Cound = '1' then
					Sreg0 <= S5;
				elsif Cound = '0' then
					Sreg0 <= S3;
				end if;
			when S4 =>
				if Cound = '1' then
					Sreg0 <= S5;
				elsif Cound = '0' then
					Sreg0 <= S0;
				end if;
			when S1 =>
				if Cound = '1' then
					Sreg0 <= S5;
				elsif Cound = '0' then
					Sreg0 <= S2;
				end if;
			when S5 =>
				if Cound = '1' then
					Sreg0 <= S5;
				elsif Cound = '0' then
					Sreg0 <= S0;
				end if;
			when S0 =>
				if Cound = '1' then
					Sreg0 <= S1;
				elsif Cound = '0' then
					Sreg0 <= S0;
				end if;
			when S3 =>
				if Cound = '1' then
					Sreg0 <= S5;
				elsif Cound = '0' then
					Sreg0 <= S4;
				end if;
--vhdl_cover_off
			when others =>
				null;
--vhdl_cover_on
		end case;
	end if;
end process;

-- signal assignment statements for combinatorial outputs
Scan_assignment:
Scan <= "0010" when (Sreg0 = S2) else
        "1000" when (Sreg0 = S4) else
        "0001" when (Sreg0 = S1) else
        "1111" when (Sreg0 = S0) else
        "0100" when (Sreg0 = S3) else
        "1111";

KeyCode_assignment:
KeyCode <= Ret &Scan when (Sreg0 = S5) else
           "00000000" when (Sreg0 = S0) else
           "00000000";

Strobe_assignment:
Strobe <= '1' when (Sreg0 = S5) else
          '0' when (Sreg0 = S0) else
          '0';

Cound_assignment:
Cound <= Ret(0) or Ret(1) or Ret(2) or Ret(3);

end ScanerKey_arch;
