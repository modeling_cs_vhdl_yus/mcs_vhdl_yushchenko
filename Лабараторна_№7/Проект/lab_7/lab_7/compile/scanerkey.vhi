component ScanerKey
	port (
		CLK: in STD_LOGIC;
		Ret: in STD_LOGIC_VECTOR (3 downto 0);
		KeyCode: out STD_LOGIC_VECTOR (7 downto 0);
		Strobe: out STD_LOGIC;
		Scan: inout STD_LOGIC_VECTOR (3 downto 0));
end component;


instance_name : ScanerKey
( CLK => ,
 KeyCode => ,
 Ret => ,
 Scan => ,
 Strobe => );
