-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab_7
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab_7\lab_7\compile\scanr.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_signed.all;

entity scanr is 
	port (
		CLK: in STD_LOGIC;
		Ret: in STD_LOGIC_VECTOR (3 downto 0);
		KeyCode: out STD_LOGIC_VECTOR (7 downto 0);
		Strobe: out STD_LOGIC;
		Scan: inout STD_LOGIC_VECTOR (3 downto 0));
end scanr;

architecture scanr_arch of scanr is

-- diagram signals declarations
signal Cound: STD_LOGIC;

-- SYMBOLIC ENCODED state machine: Sreg0
type Sreg0_type is (
    S0, S1, S2, S3, S4, S5
);
-- attribute ENUM_ENCODING of Sreg0_type: type is ... -- enum_encoding attribute is not supported for symbolic encoding

signal Sreg0: Sreg0_type;

begin

-- concurrent signals assignments

-- Diagram ACTION

----------------------------------------------------------------------
-- Machine: Sreg0
----------------------------------------------------------------------
Sreg0_machine: process (CLK)
begin
	if rising_edge(CLK) then
		-- Set default values for outputs, signals and variables
		-- ...
		case Sreg0 is
			when S0 =>
				if Cound = '1' then
					Sreg0 <= S1;
				elsif Cound = '0' then
					Sreg0 <= S0;
				end if;
			when S1 =>
				if Cound = '0' then
					Sreg0 <= S2;
				elsif Cound = '1' then
					Sreg0 <= S5;
				end if;
			when S2 =>
				if Cound = '0' then
					Sreg0 <= S3;
				elsif Cound = '1' then
					Sreg0 <= S5;
				end if;
			when S3 =>
				if Cound = '0' then
					Sreg0 <= S4;
				elsif Cound = '1' then
					Sreg0 <= S5;
				end if;
			when S4 =>
				if Cound = '0' then
					Sreg0 <= S0;
				elsif Cound = '1' then
					Sreg0 <= S5;
				end if;
			when S5 =>
				if Cound = '0' then
					Sreg0 <= S0;
				elsif Cound = '1' then
					Sreg0 <= S5;
				end if;
--vhdl_cover_off
			when others =>
				null;
--vhdl_cover_on
		end case;
	end if;
end process;

-- signal assignment statements for combinatorial outputs
Scan_assignment:
Scan <= "1111" when (Sreg0 = S0) else
        "0001" when (Sreg0 = S1) else
        "0010" when (Sreg0 = S2) else
        "0100" when (Sreg0 = S3) else
        "1000" when (Sreg0 = S4) else
        "1111";

Strobe_assignment:
Strobe <= '0' when (Sreg0 = S0) else
          '1' when (Sreg0 = S5) else
          '0';

KeyCode_assignment:
KeyCode <= "00000000" when (Sreg0 = S0) else
           Ret & Scan when (Sreg0 = S5) else
           "00000000";

Cound_assignment:
Cound <= Ret(0) or Ret(1) or Ret(2) or Ret(3);

end scanr_arch;
