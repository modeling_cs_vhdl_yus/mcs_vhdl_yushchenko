-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab_7
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : D:\My_Designs\lab_7\lab_7\compile\scaner.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity scaner is 
	port (
		CLK: in STD_LOGIC;
		Ret: in STD_LOGIC_VECTOR (3 downto 0);
		KeyCode: out STD_LOGIC_VECTOR (7 downto 0);
		Strobe: out STD_LOGIC;
		Scan: inout STD_LOGIC_VECTOR (3 downto 0));
end scaner;

architecture scaner_arch of scaner is

-- diagram signals declarations
signal Cond: STD_LOGIC;

-- BINARY ENCODED state machine: Sreg0
attribute ENUM_ENCODING: string;
type Sreg0_type is (
    S0, S1, S2, S3, S5, S4
);
attribute ENUM_ENCODING of Sreg0_type: type is
	"000 " &		-- S0
	"001 " &		-- S1
	"010 " &		-- S2
	"011 " &		-- S3
	"100 " &		-- S5
	"101" ;		-- S4

signal Sreg0: Sreg0_type;

begin


----------------------------------------------------------------------
-- Machine: Sreg0
----------------------------------------------------------------------
Sreg0_machine: process (CLK)
begin
	if rising_edge(CLK) then
		-- Set default values for outputs, signals and variables
		Cond <= Ret /= '0';
		case Sreg0 is
			when S0 =>
				if Cond = '0' then
					Sreg0 <= S0;
				elsif Cond = '1' then
					Sreg0 <= S1;
				end if;
			when S1 =>
				if Cond = '1' then
					Sreg0 <= S5;
				elsif Cond = '0' then
					Sreg0 <= S2;
				end if;
			when S2 =>
				if Cond = '1' then
					Sreg0 <= S5;
				elsif Cond = '0' then
					Sreg0 <= S3;
				end if;
			when S3 =>
				if Cond = '1' then
					Sreg0 <= S5;
				elsif Cond = '0' then
					Sreg0 <= S4;
				end if;
			when S5 =>
				if Cond = '1' then
					Sreg0 <= S5;
				elsif Cond = '0' then
					Sreg0 <= S0;
				end if;
			when S4 =>
				if Cond = '0' then
					Sreg0 <= S0;
				elsif Cond = '1' then
					Sreg0 <= S5;
				end if;
--vhdl_cover_off
			when others =>
				null;
--vhdl_cover_on
		end case;
	end if;
end process;

-- signal assignment statements for combinatorial outputs
Scan_assignment:
Scan <= "1111" when (Sreg0 = S0) else
        "0001" when (Sreg0 = S1) else
        "0010" when (Sreg0 = S2) else
        "0100" when (Sreg0 = S3) else
        "1000" when (Sreg0 = S4) else
        "1111";

Strobe_assignment:
Strobe <= '0' when (Sreg0 = S0) else
          '1' when (Sreg0 = S5) else
          '0';

KeyCode_assignment:
KeyCode <= "00000000" when (Sreg0 = S0) else
           Ret & Scan when (Sreg0 = S5) else
           "00000000";

end scaner_arch;
