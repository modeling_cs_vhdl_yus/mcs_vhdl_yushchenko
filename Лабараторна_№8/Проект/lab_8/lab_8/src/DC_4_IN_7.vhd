library IEEE;
use IEEE.STD_LOGIC_1164.all;   
use IEEE.STD_LOGIC_UNSIGNED.all;

entity DC_4_IN_7 is
	port(
		X : in STD_LOGIC_VECTOR(3 downto 0);
		Y : out STD_LOGIC_VECTOR(6 downto 0)
		);
end DC_4_IN_7;

architecture DC_4_IN_7 of DC_4_IN_7 is
	
	--type declaration
	type OutputSignals is array (0 to 10) of std_logic_vector(6 downto 0);
	
	--constants
	constant Zero : STD_LOGIC_VECTOR ( 6 downto 0)  := "1110111";
	constant One : STD_LOGIC_VECTOR (6 downto 0)    := "0100100";
	constant Two : STD_LOGIC_VECTOR (6 downto 0)    := "1011101";
	constant Three : STD_LOGIC_VECTOR (6 downto 0)  := "1101101";
	constant Four : STD_LOGIC_VECTOR (6 downto 0)   := "0101110";
	constant Five : STD_LOGIC_VECTOR (6 downto 0)   := "1101011";
	constant Six : STD_LOGIC_VECTOR (6 downto 0)    := "1111011";
	constant Seven : STD_LOGIC_VECTOR (6 downto 0)  := "0100101";
	constant Eight : STD_LOGIC_VECTOR (6 downto 0)  := "1111111";
	constant Nine : STD_LOGIC_VECTOR (6 downto 0)   := "1101111";
	constant Unknow : STD_logic_vector (6 downto 0) := "0000000";
	constant IndicatorSignals : OutputSignals := (Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Unknow);
	
begin
	process(X)																										  
		variable n : integer;
	begin
		n := Conv_integer(X);
		if n>10 then 
			n:=10; 
		end if;
		Y <= IndicatorSignals(n);
	end process;	 
	
end DC_4_IN_7;