-------------------------------------------------------------------------------
--
-- Title       : \Register\
-- Design      : lab_5
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : d:\My_Designs\lab_5\lab_5\src\Register.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {\Register\} architecture {\Register\}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;  

entity ParalelRegister is
	generic (
		 SIZE : integer := 8
		);	
	port(
		CLK : in STD_LOGIC;
		WE : in STD_LOGIC;
		RE : in STD_LOGIC;
		DATA_IN : in STD_LOGIC_VECTOR(SIZE-1 downto 0);
		DATA_OUT : out STD_LOGIC_VECTOR(SIZE-1 downto 0)
		);
end ParalelRegister;

--}} End of automatically maintained section

architecture ParalelRegister of ParalelRegister is 
begin
	
	process( CLK )
		variable rememberedByte : std_logic_vector(SIZE-1 downto 0);	
	begin  
		if rising_edge(CLK) then
			if (WE xor RE) = '1' then
				if WE = '1' then
					rememberedByte := DATA_IN;
					DATA_OUT <= (others => 'Z');
				elsif RE = '1' then
					DATA_OUT <= rememberedByte;
				end if;
			else 
				DATA_OUT <= (others => 'Z');
			end if;
		end if;
	end process;
	
end ParalelRegister;