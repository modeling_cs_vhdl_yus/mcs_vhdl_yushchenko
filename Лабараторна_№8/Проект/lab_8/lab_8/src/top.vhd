-------------------------------------------------------------------------------
--
-- Title       : top
-- Design      : lab_8
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : d:\My_Designs\lab_8\lab_8\src\top.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {top} architecture {top}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity top is
	port(
		CLK : in STD_LOGIC;
		WE : in STD_LOGIC;
		X : in STD_LOGIC_VECTOR(31 downto 0);
		LCD0 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD1 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD2 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD3 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD4 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD5 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD6 : out STD_LOGIC_VECTOR(6 downto 0);
		LCD7 : out STD_LOGIC_VECTOR(6 downto 0)
		);
end top;

--}} End of automatically maintained section

architecture top of top is
	
	component ParalelRegister is
		generic (
			SIZE : integer := 32
			);
		port(
			CLK : in STD_LOGIC;
			WE : in STD_LOGIC;
			RE : in STD_LOGIC;
			DATA_IN : in STD_LOGIC_VECTOR(SIZE-1 downto 0);
			DATA_OUT : out STD_LOGIC_VECTOR(SIZE-1 downto 0)
			);
	end component; 
	
	component DC_4_IN_7 is	
		port(
			X : in STD_LOGIC_VECTOR(3 downto 0);
			Y : out STD_LOGIC_VECTOR(6 downto 0)
			);
	end component;
	
	signal DATA_OUT : std_logic_vector(32-1 downto 0);
	signal RE : std_logic;
	
begin
	
	process ( WE ) 
	begin
		RE <= not WE;	
	end process;
	
	
	reg : ParalelRegister
	port map (CLK, WE, RE , X, DATA_OUT);
	
	LCDConv0 : DC_4_IN_7
	port map (DATA_OUT (3 downto 0), LCD0);
	
	LCDConv1 : DC_4_IN_7
	port map (DATA_OUT (7 downto 4), LCD1);
	
	LCDConv2 : DC_4_IN_7
	port map (DATA_OUT (11 downto 8), LCD2);
	
	LCDConv3 : DC_4_IN_7
	port map (DATA_OUT (15 downto 12), LCD3);
	
	LCDConv4 : DC_4_IN_7
	port map (DATA_OUT (19 downto 16), LCD4);
	
	LCDConv5 : DC_4_IN_7
	port map (DATA_OUT (23 downto 20), LCD5);
	
	LCDConv6 : DC_4_IN_7
	port map (DATA_OUT (27 downto 24), LCD6);
	
	LCDConv7 : DC_4_IN_7
	port map (DATA_OUT (31 downto 28), LCD7);					
	
end top;
