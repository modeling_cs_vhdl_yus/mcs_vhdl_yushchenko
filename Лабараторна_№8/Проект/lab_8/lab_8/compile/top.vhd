-------------------------------------------------------------------------------
--
-- Title       : 
-- Design      : lab_8
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : d:\My_Designs\lab_8\lab_8\compile\top.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library ieee;
        use ieee.std_logic_1164.all;

entity top is
  port(
       CLK : in STD_LOGIC;
       WE : in STD_LOGIC;
       X : in STD_LOGIC_VECTOR(31 downto 0);
       LCD0 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD1 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD2 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD3 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD4 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD5 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD6 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD7 : out STD_LOGIC_VECTOR(6 downto 0)
  );
end top;

architecture top of top is

---- Component declarations -----

component DC_4_IN_7
  port (
       X : in STD_LOGIC_VECTOR(3 downto 0);
       Y : out STD_LOGIC_VECTOR(6 downto 0)
  );
end component;
component ParalelRegister
  generic(
       SIZE : INTEGER := 8
  );
  port (
       CLK : in STD_LOGIC;
       DATA_IN : in STD_LOGIC_VECTOR(SIZE-1 downto 0);
       RE : in STD_LOGIC;
       WE : in STD_LOGIC;
       DATA_OUT : out STD_LOGIC_VECTOR(SIZE-1 downto 0)
  );
end component;

---- Signal declarations used on the diagram ----

signal RE : STD_LOGIC;
signal DATA_OUT : STD_LOGIC_VECTOR(32 - 1 downto 0);

begin

----  Component instantiations  ----

LCDConv0 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(0),
       X(1) => DATA_OUT(1),
       X(2) => DATA_OUT(2),
       X(3) => DATA_OUT(3),
       Y => LCD0
  );

LCDConv1 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(4),
       X(1) => DATA_OUT(5),
       X(2) => DATA_OUT(6),
       X(3) => DATA_OUT(7),
       Y => LCD1
  );

LCDConv2 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(8),
       X(1) => DATA_OUT(9),
       X(2) => DATA_OUT(10),
       X(3) => DATA_OUT(11),
       Y => LCD2
  );

LCDConv3 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(12),
       X(1) => DATA_OUT(13),
       X(2) => DATA_OUT(14),
       X(3) => DATA_OUT(15),
       Y => LCD3
  );

LCDConv4 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(16),
       X(1) => DATA_OUT(17),
       X(2) => DATA_OUT(18),
       X(3) => DATA_OUT(19),
       Y => LCD4
  );

LCDConv5 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(20),
       X(1) => DATA_OUT(21),
       X(2) => DATA_OUT(22),
       X(3) => DATA_OUT(23),
       Y => LCD5
  );

LCDConv6 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(24),
       X(1) => DATA_OUT(25),
       X(2) => DATA_OUT(26),
       X(3) => DATA_OUT(27),
       Y => LCD6
  );

LCDConv7 : DC_4_IN_7
  port map(
       X(0) => DATA_OUT(28),
       X(1) => DATA_OUT(29),
       X(2) => DATA_OUT(30),
       X(3) => DATA_OUT(31),
       Y => LCD7
  );

reg : ParalelRegister
  port map(
       CLK => CLK,
       DATA_IN => X(31 downto 0),
       DATA_OUT => DATA_OUT(32 - 1 downto 0),
       RE => RE,
       WE => WE
  );


end top;
