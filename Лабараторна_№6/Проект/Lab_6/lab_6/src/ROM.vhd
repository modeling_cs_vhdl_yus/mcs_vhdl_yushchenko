-------------------------------------------------------------------------------
--
-- Title       : ROM
-- Design      : lab_6
-- Author      : Kp0c
-- Company     : SYGMA. Inc
--
-------------------------------------------------------------------------------
--
-- File        : d:\My_Designs\Lab_6\lab_6\src\ROM.vhd
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ROM} architecture {ROM}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity ROM is
	 port(
		 CEO : in STD_LOGIC;
		 A : in STD_LOGIC_VECTOR(3 downto 0);
		 D : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end ROM;

--}} End of automatically maintained section

architecture ROM of ROM is
type RomData is array (0 to 15) of std_logic_vector(3 downto 0);

constant data : RomData := ("0000","0001","0010","0011",
							"0100","0101","0110","0111",
							"1000","1001","1010","1011",
							"1100","1101","1110","1111");
begin

	process (CEO, A) is
	
	begin
		if CEO = '1' then
			D <= data(Conv_Integer(A));	
		end if;
	end process;
	

end ROM;
